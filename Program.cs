﻿using System;
using caalhp.IcePluginAdapters;

namespace CareBedDriverFaker
{
	class Program
	{
		static void Main(string[] args)
		{
			//we assume the host is running on localhost:
			const string endpoint = "localhost";

			try
			{
				Console.WriteLine("ShimmerDriverFaker started");
				var driver = new CareBedDriverFakerImplementation();
				var adapter = new DeviceDriverAdapter(endpoint, driver);
				Console.WriteLine("ShimmerDriverFaker running");
				driver.ReadFakeInput();
			}
			catch (Exception ex)
			{
				//Connection to host probably failed
				Console.WriteLine(ex.Message);
				Console.ReadLine();
			}
		}
	}
}
