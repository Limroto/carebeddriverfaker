﻿using System;
using System.Collections.Generic;
using System.Threading;
using caalhp.Core.Contracts;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;
using CareBedDriverFaker.Service_References.CareBed;
using EventTypes;
using EventTypes.TestEvents;

namespace CareBedDriverFaker
{
	public class CareBedDriverFakerImplementation : IDeviceDriverCAALHPContract
	{
		public static string Name { get; private set; }
		public static string CareBedEvent { get; set; }
		private IDeviceDriverHostCAALHPContract _host;
		private int _processId;

		public CareBedDriverFakerImplementation()
		{
			Name = "CareBedDriverFaker";
			CareBedEvent = "Unknown";
		}

		public void Notify(KeyValuePair<string, string> notification)
		{
			//throw new System.NotImplementedException();
		}

		public string GetName()
		{
			return Name;
		}

		public bool IsAlive()
		{
			return true;
		}

		public void ShutDown()
		{
			Environment.Exit(0);
		}

		public void Initialize(IDeviceDriverHostCAALHPContract hostObj, int processId)
		{
			_host = hostObj;
			_processId = processId;
		}

		public void ReadFakeInput()
		{
			Console.WriteLine();
			Console.WriteLine("You can send fake data from this console");
			Console.WriteLine("All data is of the event type: " + CareBedEvent);
			Console.WriteLine("List of setting to send with:");
			Console.WriteLine("0: Exit faker");
			Console.WriteLine("1: In bed = true");
			Console.WriteLine("2: In bed = false");
			Console.WriteLine("3: Use the real bed");

			while (true)
			{
				var input = Console.ReadLine();

				//Create dummy event
				var bed = new DummyBed
				{
					CallerProcessId = 1,
					Condition = false,
					Data = new List<int>(),
					CallerName = "Dummy bed"
				};

				//Dertermine what to do with the event
				switch (input)
				{
					case "0":
						Console.WriteLine("Shutting down");
						ShutDown();
						break;

					case "1":
						Console.WriteLine("Send a 'In bed' event");
						bed.Condition = true;
						SendEvent(bed);
						break;

					case "2":
						Console.WriteLine("Send a 'Not in bed' event");
						bed.Condition = false;
						SendEvent(bed);
						break;

					case "3":
						Console.WriteLine("Using a real bed");
						UseRealBed();
						break;
				}

				Console.WriteLine("What now?");
			}
		}

		private void UseRealBed()
		{
			var bed = new BedWeightSensorClient();
			var latestReading = bed.BedOccupied();

			while (bed.Connected())
			{
				var isOccupied = bed.BedOccupied();
				if (latestReading != isOccupied)
				{
					latestReading = isOccupied;
					var weight = (int) bed.Weight();
					var bedEvent = new CareBed
					{
						CallerProcessId = _processId,
						Condition = bed.BedOccupied(),
						Data = new List<int> {weight},
						CallerName = "Care bed"
					};
					SendEvent(bedEvent);
				}
			}
		}

		private void SendEvent(BaseEvent eventToSend)
		{
			_host.Host.ReportEvent(EventHelper
				.CreateEvent(SerializationType.Json, eventToSend, "EventTypes"));
		}
	}
}